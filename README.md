# Journal Des Promos - Simplon
En Utilisant Python avec le framework Django.


### A. For Admin
1. See Overall Summary Charts of Stuudents Performance, Staffs Perfomrances, Courses, Subjects, Leave, etc.
2. Manage Staffs (Add, Update and Delete)
3. Manage Students (Add, Update and Delete)
4. Manage Course (Add, Update and Delete)
5. Manage Subjects (Add, Update and Delete)
6. Manage Sessions (Add, Update and Delete)
7. View Student Attendance
8. Review and Reply Student/Staff Feedback
9. Review (Approve/Reject) Student/Staff Leave

### B. For Staff
1. See the Overall Summary Charts related to their students, their subjects, leave status, etc.
2. Take/Update Students Attendance
3. Add/Update Result
4. Apply for Leave
5. Send Feedback to HOD

### C. For Students
1. See the Overall Summary Charts related to their attendance, their subjects, leave status, etc.
2. View Attendance
3. View Result
4. Apply for Leave
5. Send Feedback to HOD


### Technos used:

1. Django Framework

2. SQLite

3. DB browser for SQLite

3. Virtualenv

3. Python


### Installations

1. Create a Folder for the project**

2. Create a Virtual Environment and Activate**

Install Virtual Environment First

$  pip install virtualenv

Create Virtual Environment

$  python3 -m venv venv

Activate Virtual Environment

$  source venv/bin/activate


3. Run the project**

$  cd django-student-management-system

4. Run Server**

$ python3 manage.py runserver

5. Login Credentials**

Create Super User (HOD)

$  python manage.py createsuperuser

Then Add Email, Username and Password

**Default Credentials**

*For Admin*
Email: admin@gmail.com
Password: admin

*For Staff*
Email: staff@gmail.com
Password: staff

*For Student*
Email: student@gmail.com
Password: student


